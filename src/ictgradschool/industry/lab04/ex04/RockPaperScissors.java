package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 * A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    public static final int PAPER = 2;
    public static final int SCISSOR = 3;

    public String[] choices = {"Rock", "Paper", "Scissors", "To Quit"};
    public String playerName = "";
    public String resultString = "";

    public int playerChoice;
    public int computerChoice;
    public boolean hasWon = false;

    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.

    public void start() {

        System.out.println("Hi, what is your name? ");
        playerName = Keyboard.readInput();
        program();
    }

    public void program() {

        System.out.println(" ");
        System.out.println("Make a Choice Below:");
        System.out.println("1. Rock");
        System.out.println("2. Paper");
        System.out.println("3. Scissors");
        System.out.println("4. Quit");
        System.out.println("Enter Choice: ");


        playerChoice = Integer.parseInt(Keyboard.readInput()) - 1;
        computerChoice = (int) (Math.random() * (2));

        if (playerChoice == 3) {
            System.out.println("Quitting!");
        } else {
            displayPlayerChoice(playerName, playerChoice);
            resultString = getResultString(playerChoice, computerChoice);
            System.out.println(resultString);
            program();
        }

    }
    public void displayPlayerChoice(String name, int choice) {
        System.out.println(name + " chose " + choices[choice]);
        System.out.println("Computer has chosen " + choices[computerChoice]);
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        hasWon = false;

        if (playerChoice == 0 && computerChoice == 2) {
            hasWon = true;
        }

        if (playerChoice == 1 && computerChoice == 0) {
            hasWon = true;
        }

        if (playerChoice == 2 && computerChoice == 1) {
            hasWon = true;
        }

        return hasWon;
    }


    public String getResultString(int playerChoice, int computerChoice) {

        final String ROCK_WINS = "rock smashes scissors";
        final String PAPER_WINS = "paper covers rock";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        hasWon = userWins(playerChoice, computerChoice);

        if ((playerChoice) == computerChoice) {

            resultString = "No-one Wins";

        } else {

            if (hasWon) {

                if (playerChoice == 0) {
                    resultString = playerName + " has won because " + ROCK_WINS;
                }

                if (playerChoice == 1) {
                    resultString = playerName + " has won because " + PAPER_WINS;
                }

                if (playerChoice == 2) {
                    resultString = playerName + " has won because " + SCISSORS_WINS;
                }
            }

            if (!hasWon) {

                if (computerChoice == 0) {
                    resultString = "Computer has won because " + ROCK_WINS;
                }

                if (computerChoice == 1) {
                    resultString = "Computer has won because " + PAPER_WINS;
                }

                if (computerChoice == 2) {
                    resultString = "Computer has won because " + SCISSORS_WINS;
                }

            }

        }

        return resultString;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
