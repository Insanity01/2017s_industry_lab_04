package ictgradschool.industry.lab04.ex06;

import java.util.Objects;

public class MobilePhone {
    private String brand;
    private String model;
    private double price;

    public MobilePhone(String brand, String model, double price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    public void setBrand(String brand) {

        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString() {
        return this.model + " which cost $" + this.price;
    }

    public boolean isCheaperThan(Object other) {
        MobilePhone otherC = (MobilePhone) other;
        if (otherC.price < this.price) {

            return true;
        }

        return false;

    }

    public boolean equals(Object other) {

        if (other instanceof MobilePhone) {
            MobilePhone otherC = (MobilePhone) other;
            return this.brand.equals(otherC.brand) && this.model.equals(otherC.model) && this.price == otherC.price;
        }

        return false;
    }

    // TODO Insert equals() method here

}


