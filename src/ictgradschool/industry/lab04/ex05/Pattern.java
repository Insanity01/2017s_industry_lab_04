package ictgradschool.industry.lab04.ex05;

/**
 * Created by vwen239 on 14/11/2017.
 */
public class Pattern {
    private char patternSymbol;
    private int repetitions;

    public Pattern(int repetitions, char patternSymbol ) { // constructor
        this.repetitions = repetitions;
        this.patternSymbol = patternSymbol;
    }

    public String toString() {

        String repPattern = "";

        for (int i = 1; i <= this.repetitions; i++) {
            repPattern += patternSymbol;
        }
        return repPattern;
    }

    public void setNumberOfCharacters(int newReptition) {
        this.repetitions = newReptition;
    }

    public int getNumberOfCharacters() {
        return repetitions;
    }

}
