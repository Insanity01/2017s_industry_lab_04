package ictgradschool.industry.lab04.ex07;

import ictgradschool.industry.lab04.ex06.MobilePhone;

public class Lecturer {

    // instance variables
    private String name;
    private int staffId;
    private String[] papers;
    private boolean onLeave;

    public Lecturer(String name, int staffId, String[] papers, boolean onLeave) {

        this.name = name;
        this.staffId = staffId;
        this.papers = papers;
        this.onLeave = onLeave;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStaffId() {
        return this.staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String[] getPapers() {
        return this.papers;
    }

    public void setPapers(String[] papers) {
        this.papers = papers;
    }

    public boolean isOnLeave() {
        return this.onLeave;
    }

    public void setOnLeave(boolean onLeave) {
        this.onLeave = onLeave;

    }

    public String toString() {
        return "id:" + this.staffId + " " + name + " is teaching " + this.papers.length + " papers";

    }

    public boolean teachesMorePapersThan(Lecturer other) {

        if (this.papers.length > other.papers.length) {

            return true;
        }

        return false;

    }

}


