package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public static void start() {

        int goal;
        int guess = 0;
        goal = (int) (Math.random() * 101 + 1);

        while (guess != goal) {
            System.out.println("Please your guess between 1 and 100");
            guess = Integer.parseInt(Keyboard.readInput());

            if (guess > goal) {
                System.out.println("Too high, try again");
            }

            if (guess < goal) {
                    System.out.println("Too low, try again");
                }
            else {

                if (guess == goal) {
                    System.out.println("Perfect!");
                    System.out.println("Goodbye!");
                }
            }

        }

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
